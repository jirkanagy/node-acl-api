const response = require('./../helpers/response')
const aclRoles = require('./../const/aclRoles')

//Acl callback from express routing
module.exports.check = aclCheck = (route, req, res) => {

  //default role set to guest
  const currentUser = req.user || { role: aclRoles.guest }

  if (route.role.indexOf(currentUser.role) === -1) {

    response.forbidden(res)
    return
  }

  route.execute(req, res);

}