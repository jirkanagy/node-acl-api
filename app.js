//Imports
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const foreach = require('foreach')
const passport = require('passport')

//Custom lib
const aclLib = require('./lib/acl')
const routes = require('./route/index');
const routerHelper = require('./helpers/route')
const dateHelper = require('./helpers/date')
const response = require('./helpers/response')

//configuration
require('./config/passport')
const sessionConfig = require('./config/session')



//Env definitions
const environment = process.env.ENV || 'dev'
const virtualHost = process.env.VIRTUAL_HOST || 'localhost'
const virtualPort = process.env.VIRTUAL_PORT || 3000


//Starting express setup
console.log('\n\n\nExpress setup');
console.log('------------------');

//Express instance
const app = express()

//Express configuration
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(sessionConfig)
app.use(passport.initialize())
app.use(passport.session())

//Acl express definition with routes
foreach(routes, (module) => {

  let modulePrefix = module.prefix

  foreach(module.routes, (route) => {

      const fullPath = routerHelper.createCompletePath(modulePrefix, route.path)
      const method = route.method || 'get'
      const aclCheck = aclLib.check

      //create express routing from route definition
      if (method === 'get') {

        app.get(fullPath, (req, res) => {
          aclCheck(route, req, res)
        })

      } else if (method === 'post') {

        app.post(fullPath, (req, res) => {
          aclCheck(route, req, res)
        })

      } else if (method === 'put') {

        app.put(fullPath, (req, res) => {
          aclCheck(route, req, res)
        })

      } else if (method === 'delete') {

        app.delete(fullPath, (req, res) => {
          aclCheck(route, req, res)
        })

      } else {

        throw {
          message: 'Route `' + fullPath + '` use unsupported method `' + method + '`.\n ' +
          'Allowed are only (get|post|put|delete).'
        }
      }

      //Printing route map before start
      console.log('Route registered : [' + method + '] ' + fullPath)
    }
  )
})


// Catch all others
app.all('*', (req, res) => {
  response.notFound(res)
})

// Starting express server
app.listen(virtualPort, (err) => {

  if (err) {
    throw err
  }

  console.log('\n======================================')
  console.log('=  Server ready ' + dateHelper.databaseFormat(new Date()) + '  =')
  console.log('======================================')
  console.log('ENV: ' + environment)
  console.log('PORT: ' + virtualPort)
  console.log('URL: http://' + virtualHost)
  console.log('--------------------------------------')

})
