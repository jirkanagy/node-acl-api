const errorAuthFailed = { status: 401, code: 401, message: 'Authentication failed'}
const errorForbidden = { status: 403, code: 403, message: 'Forbidden'}
const errorNotFound = { status: 404, code: 404, message: 'Not found'}

//default error message
const errorResponse = (res, error) => {

  const responseBody = {
    state: 'error',
    errorCode: error.code,
    errorMessage: error.message
  }

  res.status(error.status).send(responseBody)
}


//success response
module.exports.success = (res, body = null) => {


  let responseBody = {
    state: 'success',
  }

  if(body) {
    responseBody.body = body;
  }

  res.status(200).send(responseBody)
}

//Error responses
module.exports.authenticationFailed = (res) => {errorResponse(res, errorAuthFailed )}
module.exports.forbidden = (res) => {errorResponse(res, errorForbidden )}
module.exports.notFound = (res) => {errorResponse(res, errorNotFound )}