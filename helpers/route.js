//Imports
const foreach = require('foreach')

//Complete module path with prefix
module.exports.createCompletePath = (modulePrefix, routePath) => {
  return '/' + modulePrefix + '/' + routePath
}

//return array of params from route path
// /user/:username-:id => ['username', 'id']
module.exports.getParamsFromPath = (path, full = false) => {


  let variableRegExp = new RegExp(/:([A-z]*)/g)
  let params = path.match(variableRegExp);

  if (full) {
    if (params) {
      return params
    }

    return []
  }


  let clearParams = [];

  if (params) {
    foreach(params, (variable) => {
      clearParams.push(variable.replace(':', ''))
    });
  }

  return clearParams
}