const guest = 'GUEST'
const user = 'USER'
const admin = 'ADMIN'

//export roles
module.exports.guest = guest
module.exports.user = user
module.exports.admin = admin
