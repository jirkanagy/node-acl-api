//import
const passport = require('passport')

const aclRoles = require('../../const/aclRoles')
const response = require('../../helpers/response')

//module export
module.exports = {
  name: 'login',
  path: 'login',
  method: 'post',
  role: [aclRoles.guest],
  execute: (req, res) => {

    passport.authenticate('local', function (err, user) {

      if (!user) {
        response.authenticationFailed(res)
        return;
      }

      response.success(res);

    })(req, res);

  },
}
