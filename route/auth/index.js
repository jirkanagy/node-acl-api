module.exports = [
  require('./login.get'),
  require('./login.post'),
  require('./logout.get')
]
