//import
const aclRoles = require('../../const/aclRoles')
const response = require('../../helpers/response')

//module export
module.exports = {
  name: 'login',
  path: 'login',
  method: 'get',
  role: [aclRoles.guest],
  execute: (req, res) => {

    response.success(res, 'hello world');

  }
}
