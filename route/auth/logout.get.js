//import
const aclRoles = require('../../const/aclRoles')
const response = require('../../helpers/response')

module.exports = {
  name: 'logout',
  path: 'logout',
  method: 'get',
  role: [aclRoles.guest, aclRoles.admin],
  execute: (req, res) => {

    req.logout()

    response.success(res)
  }
}