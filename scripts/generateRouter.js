const foreach = require('foreach')
const rh = require('../helpers/route')
const routes = require('../route/index');

console.log('\n\n' +
  '//=============================================\n' +
  '// THIS IS A GENERATED FILE, PLEASE DO NOT EDIT\n' +
  '// THIS IS HELPER FOR ROUTING API\n' +
  '//=============================================\n\n' +
  '' +
  'const routerGenerator = (route, params) => {\n' +
  '  for (let key in params) {\n' +
  '    let value = null;\n' +
  '    if (params.hasOwnProperty(key)) {\n' +
  '      value = params[key]\n' +
  '    }\n' +
  '    route = route.replace(\':\' + key, encodeURIComponent(value));\n' +
  '  }\n' +
  '  return route;\n' +
  '}' +
  '\n'
)

foreach(routes, (module, moduleName) => {

  let modulePrefix = module.prefix;

  foreach(module.routes, (route) => {

    //Route name transform into first letter uppercase
    let routeName = route.name;
    routeName = routeName.charAt(0).toUpperCase() + routeName.slice(1);

    //add module prefix
    let fullRouteName = moduleName + routeName

    //full path wit module prefix
    let completePath = rh.createCompletePath(modulePrefix, route.path);

    //get params as method arguments
    let joinedParams = rh.getParamsFromPath(route.path).join(', ');

    //create module exports name
    let moduleExportsString = 'module.exports.' + fullRouteName + ' = ' +
      '(' + joinedParams + ') => { ' +
      'return routerGenerator(\'' + completePath + '\' , {' + joinedParams + '}) }'

    console.log(moduleExportsString);

  })

});

console.log('\n' +
  '//=============================================\n' +
  '// FILE GENERATE COMPLETE\n' +
  '//=============================================\n'
)