const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

passport.serializeUser(function (user, done) {

  console.log('serialize user', user)
  done(null, user.id);

});

//session
passport.deserializeUser(function (id, done) {

  //defined user
  const user = {
    username: 'userik',
    id: 19
  }

  console.log('deserialize user', id)

  done(null, user);

});

//tady se zkusis prihlasit emailem a heslem
passport.use(new LocalStrategy(
  function(username, password, done) {

    //defined user
    const user = {
      username: 'userik',
      id: 19
    }

    console.log('authentication user:');
    console.log(user);

    return done(null, user);

  }
));

module.exports = passport;


