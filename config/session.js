const expressSession = require('express-session')

const session = expressSession({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: false
});

module.exports = session;


